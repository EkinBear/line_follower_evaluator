from __future__ import division

import enum


class EvaluatorType(enum.Enum):
    LEFTTRACK = 0
    RIGHTTRACK = 1
    LINETRACK = 2
    SHARPLEFT = 3
    SHARPRIGHT = 4