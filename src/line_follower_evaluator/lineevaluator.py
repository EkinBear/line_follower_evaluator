from __future__ import division

import math
from math import sin, cos, pi, hypot, atan2
from angles import normalize_angle, shortest_angular_distance
from geometry_msgs.msg import Point


class LineEvaluator:

    def __init__(self, tile_x, tile_y, tile_theta):

        self.evaluator_type = 'LINE'
        self.tile_x = tile_x
        self.tile_y = tile_y
        self.tile_theta = tile_theta

        self.tile_distance = 1.0
        self.tile_sum_theta = 0.0

        # position and prev
        self.position = Point()
        self.prev_position = Point()

        self.sum_area = 0

    def update(self, position, prev_position):

        self.position = position
        self.prev_position = prev_position

        if -0.01 < self.tile_theta < 0.01:
            error = self.tile_x - self.position.x
            distance = self.prev_position.y - self.position.y
        elif -1.58 < self.tile_theta < -1.56:
            error = self.tile_y - self.position.y
            distance = self.prev_position.x - self.position.x
        elif 3.13 < self.tile_theta < 3.15:
            error = self.tile_x - self.position.x
            distance = self.position.y - self.prev_position.y
        elif 1.56 < self.tile_theta < 1.58:
            error = self.tile_y - self.position.y
            distance = self.position.x - self.prev_position.x

        self.sum_area += math.fabs(error * distance)

    def get_sum_area(self):
        return self.sum_area

    def get_character(self):
        return self.tile_distance, self.tile_sum_theta
