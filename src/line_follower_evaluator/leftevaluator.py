from __future__ import division

import math
from math import sin, cos, pi, hypot, atan2, pow
from angles import normalize_angle, shortest_angular_distance
from geometry_msgs.msg import Point


def tri_area(tri):
    x1, y1, x2, y2, x3, y3 = tri[0][0], tri[0][1], tri[1][0], tri[1][1], tri[2][0], tri[2][1]
    return math.fabs(0.5 * (((x2 - x1) * (y3 - y1)) - ((x3 - x1) * (y2 - y1))))


class LeftEvaluator:

    def __init__(self, tile_x, tile_y, tile_theta):

        self.evaluator_type = 'LEFT'
        self.tile_x = tile_x
        self.tile_y = tile_y
        self.tile_theta = tile_theta

        self.tile_distance = 0.785398
        self.tile_sum_theta = 1.570796

        # radius is 0.5m
        self.r = 0.5

        # position and prev
        self.position = Point()
        self.prev_position = Point()
        self.center_point = Point()

        self.sum_area = 0

        # given r, and tile positions, calculate the center_point of the circle depending on theta
        if -0.01 < self.tile_theta < 0.01:
            self.center_point.x = self.tile_x + self.r
            self.center_point.y = self.tile_y - self.r
        elif -1.58 < self.tile_theta < -1.56:
            self.center_point.x = self.tile_x - self.r
            self.center_point.y = self.tile_y - self.r
        elif 3.13 < self.tile_theta < 3.15:
            self.center_point.x = self.tile_x - self.r
            self.center_point.y = self.tile_y + self.r
        elif 1.56 < self.tile_theta < 1.58:
            self.center_point.x = self.tile_x + self.r
            self.center_point.y = self.tile_y + self.r

    def update(self, position, prev_position):

        self.position = position
        self.prev_position = prev_position

        tri = [[self.center_point.x, self.center_point.y], [self.position.x, self.position.y], [self.prev_position.x, self.prev_position.y]]
        phi = atan2(self.position.y - self.center_point.y, self.position.x - self.center_point.x)
        prev_phi = atan2(self.prev_position.y - self.center_point.y, self.prev_position.x - self.center_point.x)

        circle_area = math.fabs((normalize_angle(phi - prev_phi) * math.pow(self.r, 2)) / 2)
        triangle_area = tri_area(tri)

        self.sum_area += math.fabs(triangle_area - circle_area)

    def get_sum_area(self):
        return self.sum_area

    def get_character(self):
        return self.tile_distance, self.tile_sum_theta
